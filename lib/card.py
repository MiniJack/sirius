from lib import mouse
import pygame
#Alligns co-ords
def allign(x,y,mouseX,mouseY,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY):

    currentX = gridPosX
    currentY = gridPosY

    for j in range(amountHeight):
        for i in range(amountWidth):
            #If mouse in grid then movement then card can be alligned
            if mouse.detect(mouseX,mouseY,gridPosX,gridPosX + gridWidth,gridPosY,gridPosY + gridHeight):

                if mouse.detect(mouseX,mouseY,currentX,currentX + marginX,currentY,currentY + marginY):
                    x,y = currentX,currentY
                    return x,y

            currentX += marginX

        #Reset X value
        currentX = gridPosX

        #Increment Y value
        currentY += marginY

    return x,y

def pickup(images,mouseX,mouseY,marginX,marginY,MovingCard,playerTurn,dragging):

    #Loop through images and detect if mouse is over a card and if so begin moving it
    isTrue = False

    for i in images:

        #If card in a players hand
        if mouse.detect(mouseX,mouseY,i.x,i.x + marginX,i.y,i.y + marginY) and i.cardPlayed == False and i.player == playerTurn and not MovingCard and i.mainShip == False:
            i.dragging = True
            MovingCard = True
            isTrue = True
            dragging = i
            return images, MovingCard, dragging

    if not isTrue:
        return images,MovingCard, dragging

def drop(img,mouseX,mouseY,MovingCard,images,gridPosX,gridPosY,gridWidth,gridHeight,amountWidth, amountHeight,marginX,marginY,cardSpace,HandY,playerTurn):

    i = images.index(img)
    #Sets dragging to false,stopping the card moving.
    images[i].dragging = False
    MovingCard = False

    #Used to tell if a move is valid
    allowedPlay = False

    #If player ones turn then check their side
    if playerTurn == 1:
        if mouse.detect(images[i].x,images[i].y,gridPosX,gridPosX + (2*marginX),gridPosY,gridPosY + gridHeight):
            allowedPlay = True

    #If player twos turn then check their side
    elif playerTurn == 2:

        if mouse.detect(images[i].x,images[i].y,gridPosX + ((amountWidth-2)*marginX),gridPosX + gridWidth,gridPosY,gridPosY + gridHeight):
            allowedPlay = True

    if allowedPlay and images[i].mainShip == False:
        #allign card to grid
        images[i].x,images[i].y = allign(images[i].x,images[i].y,mouseX,mouseY,gridPosX,gridPosY, amountWidth,amountHeight,gridWidth,gridHeight,marginX,marginY)
        images[i].cardPlayed = True
        images[i].dragging = False


    else:
        images[i].dragging = False
        #print(i)

        if images[i].player == 2:
            images[i].x = ((i-6) * cardSpace) + 290
            images[i].y = HandY[playerTurn]
        else:
            images[i].x = (i * cardSpace) + 290
            images[i].y = HandY[playerTurn]

    return images,MovingCard


def attack(a,b):
    print("attack")
    #Handles unit attacks
    #Take away attack from shield
    originalHealth = b.health
    b.shield = b.shield - a.attack

    #If attack is greater than shield then take excess from health
    if b.shield < 0:
        overflow = b.shield
        b.health += overflow
        b.shield = 0

    #If health isn't full then character is damaged
    if b.health != originalHealth:
        b.damaged = True

    return a,b

def dead(images):

    for i in images:
        if i.health <= 0:
            #print("dead")
            if i.mainShip:
                del images[images.index(i)]
                return True,images

            else:
                del images[images.index(i)]

    return False,images

def overOne(images,mouseX,mouseY,marginX,marginY):
    #Checks if mouse is over any card and if so return true else return false
    img = []

    for i in images:

        if pygame.Rect(i.x,i.y,60,40).collidepoint(mouseX,mouseY):
            img.append(i)

    if not img == []:
        return True,img
    else:
        return False,img

def mouseOver(images,mouseX,mouseY,marginX,marginY):
    try:
        for i in images:
            #If mouse over a image
            if i.mainShip == True:
                if mouse.detect(mouseX,mouseY,i.x,i.x + 150,i.y,i.y + 130):

                    return True,images.index(i)
            else:


                if mouse.detect(mouseX,mouseY,i.x,i.x + marginX,i.y,i.y + marginY) and i.selected == False:
                    return True,images.index(i)

    except TypeError:
        pass
    return False,0

def dragCard(mouseX,mouseY,images):

    #Allows cards to be moved from hand
    for i in images:
        if i.dragging:
            i.x = mouseX
            i.y = mouseY

    return images

def select(i,images,mouseX,mouseY,marginX,marginY,pygame,playerTurn):
    success,img = overOne(images,mouseX,mouseY,marginX,marginY)

    #If card played and not selected and mouse is over then try to select
    if i.cardPlayed == True and mouse.detect(mouseX,mouseY,i.x,i.x + marginX,i.y,i.y + marginY) and i.player == playerTurn:
        if mouse.click(pygame.event.poll(),pygame) and len(img) == 1:
            i.selected = True
    return images

def deselect(i,mouseX,mouseY,marginX,marginY):

    #If card selected try to deselect it.
    if mouse.detect(mouseX,mouseY,i.x,i.x + marginX,i.y,i.y + marginY):
        if mouse.click(pygame.event.poll(),pygame):
            i.selected = False
    return i

def validMove(images,i,pygame,mouseX,mouseY,marginX,marginY,gridPosX,amountWidth,playerTurn):
    #Checks if card is the correct players and selected and the user clicks the mouse
    if i.selected and mouse.click(pygame.event.poll(),pygame) == True and i.player == playerTurn:

        #Used to make sure cards can't move backwards
        success,index = mouseOver(images,mouseX,mouseY,marginX,marginY)

        #If player ones card
        if i.player == 1:

            #If player is over main ship and ship is able to attack it
            if success and images[index].mainShip == True:

                #If card can attack main ship
                if  i.x == gridPosX + ((amountWidth-1) * marginX):
                    return True

            else:

                print("here")
                #Validate movement for ship
                if mouse.detect(mouseX,mouseY,i.x,i.x + (2*marginX),i.y - marginY,i.y + (2*marginY)) ==  True:
                    return True

        #If player twos card
        elif i.player == 2:

            #If player is over main ship and ship is able to attack it
            if success and images[index].mainShip == True:

                if i.x == gridPosX:
                    return True

            else:
                #Validate movement for ship
                if mouse.detect(mouseX,mouseY,i.x - marginX,i.x + marginX,i.y - marginY,i.y + (2*marginY)) ==  True:
                    return True

            return False

def isFriendly(a,b):

    #Checks if card is friendly

    if a.player == b.player:
        return True
    else:
        return False

def move(i,mouseX,mouseY,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY):
    i.x,i.y = allign(i.x,i.y,mouseX,mouseY,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY)
    i.selected = False

    #Card has now moved
    i.hasMoved = True

    return i

def emptyTile(images,mouseX,mouseY,marginX,marginY):

    success,Index = mouseOver(images,mouseX,mouseY,marginX,marginY)

    if images[Index].mainShip == True:
        return False

    else:

        if success:
            return False
        else:
            return True

def movePlayedCard(mouseX,mouseY,images,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY,playerTurn,pygame):

    #Gets index of any card that mouse is over
    successCard,MouseOverIndex = mouseOver(images,mouseX,mouseY,marginX,marginY)
    successSel,cardSelIndex = selected(images)

    #If no card is selected then try to select one
    if successSel == False:
        #Allows user to select a card
        try:
            images = select(images[MouseOverIndex],images,mouseX,mouseY,marginX,marginY,pygame,playerTurn)
        except IndexError:
            images = []

    #If card selected then try to deselect
    if successSel:
        #Allows user to deselect cards
        images[cardSelIndex] = deselect(images[cardSelIndex],mouseX,mouseY,marginX,marginY)

    #If user has clicked in a valid spot
    if validMove(images,images[cardSelIndex],pygame,mouseX,mouseY,marginX,marginY,gridPosX,amountWidth,playerTurn):

        #If user has clicked on a empty tile then move card
        if emptyTile(images,mouseX,mouseY,marginX,marginY):

            #If card hasn't already moved this turn then move it unless it is the main ship
            if images[cardSelIndex].hasMoved == False:
                images[cardSelIndex] = move(images[cardSelIndex],mouseX,mouseY,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY)

            else:
                #print("Cannot move there.")
                images[cardSelIndex].selected = False

        #If tile not empty
        else:

            #Checks if card is friendly(if spot is not empty)
            if isFriendly(images[cardSelIndex],images[MouseOverIndex]):
                #If so then deselect card as invalid move
                images[cardSelIndex] = deselect(images[cardSelIndex],mouseX,mouseY,marginX,marginY)

            else:

                #If card hasn't attacked then attack
                if images[cardSelIndex].hasAttacked == False:

                    images[cardSelIndex],images[MouseOverIndex] = attack(images[cardSelIndex],images[MouseOverIndex])
                    images[cardSelIndex].hasAttacked = True
                    images[cardSelIndex].selected = False

                else:
                    #print("Card has already attacked.")
                    pass

    return images

def selected(images):
    #If any card selected return true else return false
    for i in images:
        if i.selected == True:
            return True,images.index(i)
    return False,0

def endTurn(images,mouseX,mouseY,width,height,playerTurn,attackShip,defenceShip,HandX,HandY,cardSpace):
    if mouse.detect(mouseX,mouseY,width - 150,width - 20 ,height - 100,height - 50):

        if playerTurn == 1:
            playerTurn = 2
            #print("Turn is now player: " + str(playerTurn))
        elif playerTurn == 2:
            playerTurn = 1
            #print("Turn is now player: " + str(playerTurn))


        #Reset cards move/attack limiters as turn has changed
        for i in images:
            if i.mainShip == False:
                if i.hasAttacked == True:
                    i.hasAttacked = False

                if i.hasMoved == True:
                    i.hasMoved = False

        return True,playerTurn

    return False,playerTurn

