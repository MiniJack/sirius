import pygame
from lib import card
pygame.quit()
RED = ((255,0,0))
BLUE = ((0,0,255))
GREEN = ((0,255,0))
BLACK = ((0,0,0))
def Board(marginX,marginY,height,width,screen):
    amountHeight = 7
    amountWidth = 10

    #Draw grid
    gridHeight = amountHeight * marginY
    gridWidth = amountWidth * marginX

    gridPosX = (width - gridWidth) // 2
    gridPosY = (height - gridHeight) // 2

    #Vertical
    for i in range(11):
        pygame.gfxdraw.box(screen,(gridPosX,gridPosY,2,gridHeight),BLACK)
        gridPosX += marginX

    gridPosX = (width - gridWidth) // 2
    gridPosY = (height - gridHeight) // 2

    #Horizontal
    for i in range(8):
        pygame.gfxdraw.box(screen,(gridPosX,gridPosY,gridWidth,2),BLACK)
        gridPosY += marginY

    gridPosX = (width - gridWidth) // 2
    gridPosY = (height - gridHeight) // 2

    return gridPosX,gridPosY,gridWidth,gridHeight, amountWidth, amountHeight

def Image(mouseX,mouseY,height,width,images,screen,marginX,marginY,playerTurn):

    #Font definition used for card stats
    font = pygame.font.SysFont("monospace", 20)

    #Stat Display Coords
    statDisX = 10
    statDisY = height - (height // 6)


    label = font.render("Attack Health Shield", 1, (0,0,255))
    screen.blit(label,(statDisX,statDisY))

    #Draws images and card stats
    for i in images:
        #Blit all images in array
        screen.blit(i.image,(i.x,i.y))

        #End turn button (placeholder)
        pygame.draw.rect(screen,RED,(width - 150,height - 100,100,50))

        label = font.render("Player Turn: " +str(playerTurn), 1, (0,0,255))
        screen.blit(label,(width - 200 ,height // 15))

        turn = font.render("End Turn", 1, (0,0,0))
        screen.blit(turn,(width - 150, 520))

        #Check if a card is selected
        cardSel,cardSelNum = card.selected(images)

        #If mouse over a single card
        isImg,img = card.overOne(images,mouseX,mouseY,marginX,marginY)

        if i.damaged == True:
            i.colour = RED

        else:
            i.colour = BLUE

        #If mouse over card
        overCard,cardOverNum = card.mouseOver(images,mouseX,mouseY,150,130)

        if isImg and len(img) == 1 and not cardSel and img[0].mainShip == False:

            #Blit card stats

            #Blit Attack coords relative to images
            label = font.render(str(img[0].attack), 1, (0,0,255))
            screen.blit(label,(statDisX + 25,statDisY + 60))

            #Same but for health
            label = font.render(str(img[0].health), 1, img[0].colour)
            screen.blit(label,(statDisX + 110,statDisY + 60))#

            #Same but for Shield
            label = font.render(str(img[0].shield), 1, (0,0,255))
            screen.blit(label,(statDisX + 195,statDisY + 60))

        elif images[cardOverNum].mainShip == True:

            #Blit card stats

            #Blit Attack coords relative to images
            label = font.render(str(images[cardOverNum].attack), 1, (0,0,255))
            screen.blit(label,(statDisX + 25,statDisY + 60))

            #Same but for health
            label = font.render(str(images[cardOverNum].health), 1, (0,0,255))
            screen.blit(label,(statDisX + 110,statDisY + 60))#

            #Same but for Shield
            label = font.render(str(images[cardOverNum].shield), 1, (0,0,255))
            screen.blit(label,(statDisX + 195,statDisY + 60))



        if cardSel and images[cardOverNum].mainShip == False:

            #Highlight selected card for user
            pygame.draw.rect(screen,RED,(images[cardSelNum].x,images[cardSelNum].y,marginX,marginY),2)

            #Blit selected card stats


            #Blit Attack coords relative to images
            label = font.render(str(images[cardSelNum].attack), 1, BLUE)
            screen.blit(label,(statDisX + 25,statDisY + 60))


            #Same but for health
            label = font.render(str(images[cardSelNum].health), 1, images[cardSelNum].colour)
            screen.blit(label,(statDisX + 110,statDisY + 60))


            #Same but for Shield
            label = font.render(str(images[cardSelNum].shield), 1, BLUE)
            screen.blit(label,(statDisX + 195,statDisY + 60))
