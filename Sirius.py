#Sirius
global playerTurn, images
from lib import drawing, card, mouse
import pygame,random,pygame.gfxdraw
from pygame.locals import *

pygame.init()

pygame.key.set_repeat(50,500)

    #Used to load images to be drawn when needed later

class Null:
    def __init__(self):
        pass

class image:
    #Constructor  to create image object
    def __init__(self,image,x,y,player,attack,health,shield,name,isMain):
        #Images file
        self.image = image
        self.name = name
        #Stores position of image
        self.x = x
        self.y = y

        #Used to store data about cards
        self.dragging = False
        self.selected = False
        self.cardPlayed = False

        #Stores whose card it is
        self.player = player

        #Card Stats
        self.attack = attack
        self.health = health
        self.shield = shield

        #Card stat information
        self.damaged = False
        self.colour = RED

        if isMain == True:

            #card move/attack limiters
            self.hasAttacked = True
            self.hasMoved = True

        else:

            #card move/attack limiters
            self.hasAttacked = False
            self.hasMoved = False


        #Used to tell if ship is main ship
        self.mainShip = isMain


    #Transparent image loading
    def LoadFile_alpha(file):
        a = pygame.image.load(file).convert_alpha()
        return a


    #Non-Transparent image loading
    def LoadFile(file):
        a = pygame.image.load(file).convert()
        return a


    #Call to create images
    def create(i,x,y,player,attack,health,shield,images,name,mainShip):
        a = image(i,x,y,player,attack,health,shield,name,mainShip)
        images.append(a)
        return images


'''class mouse:
    def detect(x,y,a,b,c,d):
        if x in range(a,b) and y in range(c,d):
            return True
        else:
            return False


    def click():
        #If mouse pressed return true else return false
        event = pygame.event.poll()

        if event.type == pygame.MOUSEBUTTONDOWN:
            print("clicked")
            return True
        else:
            return False


    def release():
        #If mouse released then return true else false
        event = pygame.event.poll()

        if event.type == pygame.MOUSEBUTTONUP:
            print("release")
            return True
        else:
            return False

'''

class main:
    def startMatch(images):
        for i in range(6):
            card = defenceShip


            #Create images for player one
            images = image.create(card.img,HandX[i],550,1,card.HP,card.AP,card.SH,images,card.name,False)

        for i in range(6):

            card = attackShip
            images = image.create(card.img,HandX[i],50,2,card.HP,card.AP,card.SH,images,card.name,False)

        mainShip = image.LoadFile_alpha("assets/mainShip.png")
        images = image.create(mainShip,0,height // 2 - 60,1,15,15,15,images,"Main Ship",True)

        mainShipTwo = image.LoadFile_alpha("assets/mainShipTwo.png")
        images = image.create(mainShipTwo,width - 150,height // 2 - 60,2,15,15,15,images,"Main Ship",True)



        #Once card is not in deck delete from deck including stats

        return images

    def main(images,MovingCard,playerTurn):
        global mouseX,mouseY

        #Sets up player starting hand
        images = main.startMatch(images)

        dragging = None

        while True:
            screen.fill((255,255,255))
            mouseX,mouseY = pygame.mouse.get_pos()

            gridPosX,gridPosY,gridWidth,gridHeight, amountWidth, amountHeight = drawing.Board(marginX,marginY,height,width,screen)

            #Handles movement of cards
            images = card.dragCard(mouseX,mouseY,images)


            images = card.movePlayedCard(mouseX,mouseY,images,gridPosX,gridPosY, amountWidth, amountHeight, gridWidth,gridHeight,marginX,marginY,playerTurn,pygame)

            success,img = card.overOne(images,mouseX,mouseY,marginX,marginY)

            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()

                isImgOver, imgOver = card.overOne(images,mouseX,mouseY,marginX,marginY)
                justUp = False

                #If user isn't holding a card then try to pickup card
                if event.type == MOUSEBUTTONDOWN and not MovingCard:
                    images,MovingCard,dragging = card.pickup(images,mouseX,mouseY,marginX,marginY,MovingCard,playerTurn,dragging)
                    justUp = True

                #Elif they are moving card then try to drop it
                elif event.type == MOUSEBUTTONDOWN and isImgOver and not justUp:
                    images,MovingCard = card.drop(dragging,mouseX,mouseY,MovingCard,images,gridPosX,gridPosY,gridWidth,gridHeight, amountWidth, amountHeight,marginX,marginY,cardSpace,HandY,playerTurn)

                if event.type == MOUSEBUTTONDOWN:
                    #check if user has ended
                    turnChanged,playerTurn = card.endTurn(images,mouseX,mouseY,width,height,playerTurn,attackShip,defenceShip,HandX,HandY,cardSpace)

            #Draws board and images

            drawing.Image(mouseX,mouseY,height,width,images,screen,marginX,marginY,playerTurn)
            #Check if any card is dead

            gameOver,images = card.dead(images)

            #If a player won
            if gameOver:

                break;

            #Update display
            pygame.display.update()
            pygame.display.flip()

            clock.tick(tick)

#-- TEMP --#
def rnd(a):
    return random.randint(1,a)
#--      --#

#Initialize vars


#Variable definition
(width,height) = 900,600
screen = pygame.display.set_mode((width,height))

pygame.display.set_caption("test")


#Define Clock
clock = pygame.time.Clock()
tick = 60


#Define Colours



#Stores Textures for Cards
defenceShip = Null()
attackShip = Null()

defenceShip.name = "Defence Ship"
attackShip.name = "Attack Ship"

defenceShip.img = image.LoadFile_alpha("assets/DefenceShip.png")
attackShip.img = image.LoadFile_alpha("assets/AttackShip.png")

defenceShip.HP = 7
defenceShip.AP = 4
defenceShip.SH = 5

attackShip.HP = 6
attackShip.AP = 5
attackShip.SH = 2


#Used in creation of cards
''' depricated
p1Cards = [defenceShip,defenceShip,defenceShip,defenceShip,attackShip,attackShip,attackShip,attackShip]
p1Attack = [rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7)]
p1Health = [rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7)]
p1Shield = [rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5)]


p2Cards = [defenceShip,defenceShip,defenceShip,defenceShip,attackShip,attackShip,attackShip,attackShip]
p2Attack = [rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7)]
p2Health = [rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7),rnd(7)]
p2Shield = [rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5),rnd(5)]'''



p1Cards = []

#Used to keep track of whoose turn it is
playerTurn = 1


#Stores any image to be drawn to screen
images = []
MovingCard = False


#coords of player hand
HandY = [None,550,50]

HandX = [290,360,430,500,570,640,290,360,430,500,570,640]
cardSpace = 70

#Used to draw grid
marginX = 60
marginY = 40


#Colours
RED = ((255,0,0))
BLUE = ((0,0,255))
GREEN = ((0,255,0))
BLACK = ((0,0,0))

p1Hand = []
p2Hand = []

while True:
    main.main(images,MovingCard,playerTurn)

    #Clears board and hand
    for i in images:
        del i

    images = []

